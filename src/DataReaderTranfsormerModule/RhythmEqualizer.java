package DataReaderTranfsormerModule;
import java.util.ArrayList;

public class RhythmEqualizer {
	
	public void equalizeRhythms(ArrayList<Melody> melodies){
		for(Melody m1 : melodies){
			System.out.println("rhythmCounter operation");
			equalizeRhythm(m1);
		}
    }

	public void addToEqualizedDataLists(Melody addTo, Melody addFrom, String rhythmValue){
		addTo.melody_RhythmEqualized.add(addFrom.melody.get(0));
		addTo.melodyOctave_RhythmEqualized.add(addFrom.melodyOctave.get(0));
		addTo.melodyRhythm_RhythmEqualized.add(rhythmValue);
		
		addTo.harmony_RhythmEqualized.add(addFrom.harmony.get(0));				
		addTo.harmonyOctave_RhythmEqualized.add(addFrom.harmonyOctave.get(0));
		addTo.harmonyRhythm_RhythmEqualized.add(rhythmValue);
	}
	

	public void equalizeRhythm(Melody m){
		MusicDataMapOneOctave map = new MusicDataMapOneOctave();
		// Temporary melody for subtracting rhythms.
		Melody m1 				= new Melody();
		m1.melody 				= new ArrayList<String>(m.melody);
		m1.harmony 				= new ArrayList<String>(m.harmony);
		m1.melodyOctave 		= new ArrayList<String>(m.melodyOctave);
		m1.harmonyOctave		= new ArrayList<String>(m.harmonyOctave);
		m1.melodyRhythmNumbers 	= new ArrayList<Double>(m.melodyRhythmNumbers);
		m1.harmonyRhythmNumbers = new ArrayList<Double>(m.harmonyRhythmNumbers);
		
		while(m1.melody.size() > 0){
			double difference = m1.melodyRhythmNumbers.get(0) - m1.harmonyRhythmNumbers.get(0);
			
			// If the melody rhythm value is greater then the harmony rhythm value
			if(m1.melodyRhythmNumbers.get(0) > m1.harmonyRhythmNumbers.get(0) && Math.abs(difference) > 0.00000001){
				// If the rhythm values can not be cleanly subtracted
				if(map.getRhythmNameByValue(m1.harmonyRhythmNumbers.get(0)) == null){
					double nextSmallestKey = map.getNextSmallestRhythmByValue(m1.harmonyRhythmNumbers.get(0));
					double hDifference = m1.harmonyRhythmNumbers.get(0) - nextSmallestKey;
					difference = nextSmallestKey - m1.melodyRhythmNumbers.get(0);
					String rhythmValue = map.getRhythmNameByValue(nextSmallestKey);
					
					addToEqualizedDataLists(m, m1, rhythmValue);
					
					// Replace the previous rhythm with the subtracted one
					m1.harmonyRhythmNumbers.set(0, Math.abs(hDifference));
					m1.melodyRhythmNumbers.set(0, Math.abs(difference));
				}
				// If the rhythm can be cleanly subtracted
				else {
					String rhythmValue = map.getRhythmNameByValue(m1.harmonyRhythmNumbers.get(0));
					
					addToEqualizedDataLists(m, m1, rhythmValue);										
					
					// Replace the previous rhythm with the subtracted one
					m1.melodyRhythmNumbers.set(0, Math.abs(difference));

					// Remove elements
					m1.harmony.remove(0);
					m1.harmonyOctave.remove(0);
					m1.harmonyRhythmNumbers.remove(0);
				}		
			}
			// If the harmony rhythm value is greater then the melody rhythm value
			else if(m1.melodyRhythmNumbers.get(0) < m1.harmonyRhythmNumbers.get(0)  && Math.abs(difference) > 0.00000001){
				// If the rhythm values can not be cleanly subtracted
				if(map.getRhythmNameByValue(m1.melodyRhythmNumbers.get(0)) == null){
					double nextSmallestKey = map.getNextSmallestRhythmByValue(m1.melodyRhythmNumbers.get(0));
					double mDifference = m1.melodyRhythmNumbers.get(0) - nextSmallestKey;
					difference = nextSmallestKey - m1.harmonyRhythmNumbers.get(0);
					String rhythmValue = map.getRhythmNameByValue(nextSmallestKey);
					
					addToEqualizedDataLists(m, m1, rhythmValue);
					
					// Replace the previous rhythm with the subtracted one
					m1.harmonyRhythmNumbers.set(0, Math.abs(difference));
					m1.melodyRhythmNumbers.set(0, mDifference);
				}
				// If the rhythm can be cleanly subtracted
				else {
					String rhythmValue = map.getRhythmNameByValue(m1.melodyRhythmNumbers.get(0));
					
					addToEqualizedDataLists(m, m1, rhythmValue);
					
					// Replace the previous rhythm with the subtracted one
					m1.harmonyRhythmNumbers.set(0, Math.abs(difference));
					
					// Remove elements
					m1.melody.remove(0);
					m1.melodyOctave.remove(0);
					m1.melodyRhythmNumbers.remove(0);
				}
			}
			//If the melody and harmony rhythm values are equal
			else {			
				String rhythmValue = map.getRhythmNameByValue(m1.melodyRhythmNumbers.get(0));
				
				addToEqualizedDataLists(m, m1, rhythmValue);

				// Remove elements
				m1.melody.remove(0);
				m1.melodyOctave.remove(0);
				m1.melodyRhythmNumbers.remove(0);
				m1.harmony.remove(0);
				m1.harmonyOctave.remove(0);
				m1.harmonyRhythmNumbers.remove(0);
			}
		}
		// If an error occurred a flag is raised and set to the melody object.
		if(m.melody_RhythmEqualized.size() != m.harmony_RhythmEqualized.size() ||
		   m.melodyRhythm_RhythmEqualized.size() != m.harmonyRhythm_RhythmEqualized.size()){
			m.setBad(true);
		}
	}
}
