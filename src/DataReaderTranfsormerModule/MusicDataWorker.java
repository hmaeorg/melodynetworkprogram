package DataReaderTranfsormerModule;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.jfugue.MusicStringRenderer;
import org.jfugue.MusicXmlParser;
import org.jfugue.Pattern;

import util.Paths;


public class MusicDataWorker {
	ArrayList<Melody> melodies = new ArrayList<Melody>();
	ArrayList<String> faultys = new ArrayList<String>();
	private String path;
	
	public void run(String outputpath) throws IOException{
		path = outputpath;
		ArrayList<String> filenames = getFilenames(Paths.getMusciXMLDir());	

		// Iterate over all the musicXML files
		for(int i = 0; i < filenames.size(); i++) {   
			File sheetMusic = new File(filenames.get(i));
			Pattern pattern = readFromXML(sheetMusic);
						
			//Read the musicXML data using Jfugue MusicXMLparser
			MusicXMLTransformer transformer = new MusicXMLTransformer();
			transformer.transform(pattern);
			
			// Create a new Melody type object to store the data read from the MusicXML file
			Melody melody = new Melody(transformer.melodyNotes, transformer.melodyRhythms, transformer.harmonyNotes, transformer.harmonyRhythms);
			melody.setName(filenames.get(i).substring(7, filenames.get(i).length()));
			
			this.melodies.add(melody);				
		}
		
		
		// Map melody musicstring values to Double values and try to transpose
		MusicDataMapOneOctave map = new MusicDataMapOneOctave();
		for(Melody m1 : this.melodies) {
			map.transposeOctave(m1, -1);
			map.transposeHarmony(m1);
			map.map(m1);
		}
		

		// Equalize melody and harmony rhythms
		RhythmEqualizer re = new RhythmEqualizer();
		re.equalizeRhythms(this.melodies);

		
		// Map the newly equalized values to Double values
		for(Melody m1 : this.melodies) {
			map.mapRhythmEqualized(m1);
		}
		
		
		// Print out data that can't be used
		for(Melody m1 : this.melodies) {
			if(m1.isBad()){
				System.out.println("Bad:	"+m1.getName());
			}
		}		
		

		writeToFile("melody");
		writeToFile("melodyRhythm");
		writeToFile("melodyOctave");
		
		writeToFile("harmony");
		writeToFile("harmonyRhythm");
		writeToFile("harmonyOctave");
		
		writeToFile("melody_Mapped");
		writeToFile("melodyOctave_Mapped");
		writeToFile("melodyRhythm_Mapped");
		
		writeToFile("harmony_Mapped");
		writeToFile("harmonyOctave_Mapped");
		writeToFile("harmonyRhythm_Mapped");	
		
		writeToFile("melody_RhythmEqualized");
		writeToFile("melodyOctave_RhythmEqualized");
		writeToFile("melodyRhythm_RhythmEqualized");
		
		writeToFile("harmony_RhythmEqualized");
		writeToFile("harmonyOctave_RhythmEqualized");
		writeToFile("harmonyRhythm_RhythmEqualized");	
		
		writeToFile("melody_Mapped_RhythmEqualized");
		writeToFile("melodyOctave_Mapped_RhythmEqualized");
		writeToFile("melodyRhythm_Mapped_RhythmEqualized");
		
		writeToFile("harmony_Mapped_RhythmEqualized");
		writeToFile("harmonyOctave_Mapped_RhythmEqualized");
		writeToFile("harmonyRhythm_Mapped_RhythmEqualized");
		writeToFile("songNames");
		
		// Print out data that can't be used
		for(Melody m1 : this.melodies) {
			if(m1.isBad()){
				System.out.println("Bad data:	"+ m1.getName());
			}
		}
	}
	
	
    public static ArrayList<String> getFilenames(String pathname) {
        File path = new File(pathname);
        ArrayList<String> sheetMusicAdresses = new ArrayList<String>();
        File [] files = path.listFiles();
        for (int i = 0; i < files.length; i++){
            if (files[i].isFile()){
            	sheetMusicAdresses.add(files[i].toString());
            	System.out.println(files[i].toString());
            }
        }
        Collections.sort(sheetMusicAdresses);
        return sheetMusicAdresses;
    }
    

	public Pattern readFromXML(File inputFile){
		System.out.println("readFromXML operation: " + inputFile.getName());
		MusicStringRenderer renderer = new MusicStringRenderer();
		MusicXmlParser parser = new MusicXmlParser();		
		
		parser.addParserListener(renderer);		
		parser.parse(inputFile);
		
		return renderer.getPattern();
	}

	public void writeToFile(String filename) throws IOException{
		System.out.println("writeToFile operation");
		File outputFile = new File(Paths.getCsvDir()+path+filename+".csv");
		if (!outputFile.exists()) { outputFile.createNewFile(); }
		
		FileWriter fw = new FileWriter(outputFile);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for(Melody m1 : this.melodies) {
			String csvLine = "";
			try{
				if(filename.equals("melody"))										{csvLine = generateCSVstring(m1.melody);}
				else if(filename.equals("melodyRhythm"))							{csvLine = generateCSVstring(m1.melodyRhythm);}
				else if(filename.equals("melodyOctave"))							{csvLine = generateCSVstring(m1.melodyOctave);}
				
				else if(filename.equals("harmony"))									{csvLine = generateCSVstring(m1.harmony);}
				else if(filename.equals("harmonyRhythm"))							{csvLine = generateCSVstring(m1.harmonyRhythm);}
				else if(filename.equals("harmonyOctave"))							{csvLine = generateCSVstring(m1.harmonyOctave);}
				
				else if(filename.equals("melody_Mapped"))							{csvLine = generateCSVstring(m1.melody_Mapped);}
				else if(filename.equals("melodyOctave_Mapped"))						{csvLine = generateCSVstring(m1.melodyOctave_Mapped);}
				else if(filename.equals("melodyRhythm_Mapped"))						{csvLine = generateCSVstring(m1.melodyRhythm_Mapped);}
				
				else if(filename.equals("harmony_Mapped"))							{csvLine = generateCSVstring(m1.harmony_Mapped);}
				else if(filename.equals("harmonyOctave_Mapped"))					{csvLine = generateCSVstring(m1.harmonyOctave_Mapped);}
				else if(filename.equals("harmonyRhythm_Mapped"))					{csvLine = generateCSVstring(m1.harmonyRhythm_Mapped);}
				
				else if(filename.equals("melody_RhythmEqualized"))					{csvLine = generateCSVstring(m1.melody_RhythmEqualized);}
				else if(filename.equals("melodyOctave_RhythmEqualized"))			{csvLine = generateCSVstring(m1.melodyOctave_RhythmEqualized);}
				else if(filename.equals("melodyRhythm_RhythmEqualized"))			{csvLine = generateCSVstring(m1.melodyRhythm_RhythmEqualized);}
				
				else if(filename.equals("harmony_RhythmEqualized"))					{csvLine = generateCSVstring(m1.harmony_RhythmEqualized);}
				else if(filename.equals("harmonyOctave_RhythmEqualized"))			{csvLine = generateCSVstring(m1.harmonyOctave_RhythmEqualized);}
				else if(filename.equals("harmonyRhythm_RhythmEqualized"))			{csvLine = generateCSVstring(m1.harmonyRhythm_RhythmEqualized);}
	
				else if(filename.equals("melody_Mapped_RhythmEqualized"))			{csvLine = generateCSVstring(m1.melody_Mapped_RhythmEqualized);}
				else if(filename.equals("melodyOctave_Mapped_RhythmEqualized"))		{csvLine = generateCSVstring(m1.melodyOctave_Mapped_RhythmEqualized);}
				else if(filename.equals("melodyRhythm_Mapped_RhythmEqualized"))		{csvLine = generateCSVstring(m1.melodyRhythm_Mapped_RhythmEqualized);}
				
				else if(filename.equals("harmony_Mapped_RhythmEqualized"))			{csvLine = generateCSVstring(m1.harmony_Mapped_RhythmEqualized);}			
				else if(filename.equals("harmonyOctave_Mapped_RhythmEqualized"))	{csvLine = generateCSVstring(m1.harmonyOctave_Mapped_RhythmEqualized);}		
				else if(filename.equals("harmonyRhythm_Mapped_RhythmEqualized"))	{csvLine = generateCSVstring(m1.harmonyRhythm_Mapped_RhythmEqualized);}
				else if(filename.equals("songNames"))								{csvLine = m1.getName();}
			}
			catch(Exception ex){
			}
			if(!m1.isBad()){
				bw.write(csvLine);
				bw.newLine();	
			}
		}
		bw.close();
		fw.close();
	}
	
	public String generateCSVstring(@SuppressWarnings("rawtypes") ArrayList list) throws IOException{
		StringBuilder result = new StringBuilder();
	    if (list.get(0).getClass().equals(Double.class)) {
	    	@SuppressWarnings("unchecked")
			ArrayList<Double> list1 = (ArrayList<Double>) list;
			for(double string : list1) {
				result.append(string);
				result.append(",");
			}
	    }
	    else if (list.get(0).getClass().equals(String.class)) {
	    	@SuppressWarnings("unchecked")
			ArrayList<String> list1 = (ArrayList<String>) list;
			for(String string : list1) {
				result.append(string);
				result.append(",");
			}
	    }
		return result.toString().substring(0, result.length() - 1); //eemaldame viimase koma
	}
}