package DataReaderTranfsormerModule;
import java.util.ArrayList;
import org.jfugue.PatternTransformer;
import org.jfugue.Note;
import org.jfugue.Voice;
import org.jfugue.Pattern;

public class MusicXMLTransformer extends PatternTransformer {	
	public ArrayList<String> melodyNotes = new ArrayList<String>();
	public ArrayList<String> harmonyNotes = new ArrayList<String>();
	public ArrayList<String> melodyRhythms = new ArrayList<String>();
	public ArrayList<String> harmonyRhythms = new ArrayList<String>();
	public String flag = "";
	
    public MusicXMLTransformer(){
        super();
    }

    public void noteEvent(Note note){
        insert(note.getMusicString(), " ");
        if(flag.equals("V0")){
        	// If the note doesn't have a pitch value, the it is a pause
        	if(note.getAttackVelocity() == 0 && note.getDecayVelocity() == 0){
        		melodyNotes.add("--");
                melodyRhythms.add(Note.getStringForDuration(note.getDecimalDuration()));
        	}
        	// If the note has a pitch value
        	else{
        		melodyNotes.add(Note.getStringForNote(note.getValue()));
        		melodyRhythms.add(Note.getStringForDuration(note.getDecimalDuration()));	
        	}
        }
        else if(flag.equals("V1")){
        	// If the note doesn't have a pitch value, the it is a pause
        	if(note.getAttackVelocity() == 0 && note.getDecayVelocity() == 0){
        		harmonyNotes.add("--");         
        		harmonyRhythms.add(Note.getStringForDuration(note.getDecimalDuration()));
        	}
        	// If the note has a pitch value
        	else{     
            	harmonyNotes.add(Note.getStringForNote(note.getValue()));            
            	harmonyRhythms.add(Note.getStringForDuration(note.getDecimalDuration()));
        	}
        }
        else {}     
    }
    
    public void voiceEvent(Voice voice){
    	// V0 represents Voice number 0
    	if(voice.getMusicString().equals("V0")){
    		flag = "V0";
    	}
    	// V1 represents Voice number 1
    	else if(voice.getMusicString().equals("V1")){
    		flag = "V1";
    	}
    	// Disregard other voices
    	else {}
    }
	
    private void insert(String string, String connector){
        StringBuilder noteString = new StringBuilder();
        noteString.append(getReturnPattern());
        noteString.append(string);
        noteString.append(connector);
        Pattern p = new Pattern(noteString.toString());
        
        setReturnPattern(p);        
    }
}