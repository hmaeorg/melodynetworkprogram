package DataReaderTranfsormerModule;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class MusicDataMapOneOctave extends MusicDataMap {
	public Map <String, Double> values = new HashMap<String, Double>();
    public Map <Double, String> noteValues = new HashMap<Double, String>();
    public Map <Double, String> rhythmValues = new HashMap<Double, String>();
    public Map <Double, String> octaveValues = new HashMap<Double, String>();
    public Map <String, Double> rhythmNumericValues = new HashMap<String, Double>();
	
	public MusicDataMapOneOctave(){	
		values.put("--",						0.0); 	// Pause/none        
        values.put("SPACER",					0.0);   // Spacer
		
		// Rhythm values, there are 13: 1-12 + 0 element
		values.put("s", 						1.0); 	// tiri
		values.put("/0.08333333333333333",   	2.0);	// w^3   4/4 ti  = 3/4 tiri
		values.put("i", 						3.0);	// ti
		values.put("/0.16666666666666666",   	4.0);	// w^3 	 4/4 ta  = 3/4 ti
		values.put("i.", 						5.0); 	// ti.
		values.put("q", 						6.0); 	// ta
		values.put("/0.3333333333333333",   	7.0);	// w^3   4/4 h   = 3/4 tae 
		values.put("q.", 						8.0); 	// ta.
		values.put("h", 						9.0); 	// ta-a
		values.put("/0.6666666666666666",   	10.0);	// w^3   4/4 w   = 3/4 ta-a
		values.put("h.", 						11.0); 	// ta-a.
		values.put("w", 						12.0); 	// tervik						

		
        // Pitch values, there are 13: 1-12 + 0 element
		// All octaves are matched to the same values
        values.put("C3", 	1.0);
        values.put("C#3",	2.0);
        values.put("Db3",	2.0);
        values.put("D3", 	3.0);
        values.put("D#3", 	4.0);
        values.put("Eb3", 	4.0);
        values.put("E3", 	5.0);
        values.put("F3", 	6.0);
        values.put("F#3", 	7.0);
        values.put("Gb3", 	7.0);
        values.put("G3", 	8.0);
        values.put("G#3", 	9.0);
        values.put("Ab3", 	9.0);
        values.put("A3", 	10.0);
        values.put("A#3", 	11.0);
        values.put("Bb3", 	11.0);
        values.put("B3", 	12.0);
        
        values.put("C4", 	1.0);
        values.put("C#4",	2.0);
        values.put("Db4",	2.0);
        values.put("D4", 	3.0);
        values.put("D#4", 	4.0);
        values.put("Eb4", 	4.0);
        values.put("E4", 	5.0);
        values.put("F4", 	6.0);
        values.put("F#4", 	7.0);
        values.put("Gb4", 	7.0);
        values.put("G4", 	8.0);
        values.put("G#4", 	9.0);
        values.put("Ab4", 	9.0);
        values.put("A4", 	10.0);
        values.put("A#4", 	11.0);
        values.put("Bb4", 	11.0);
        values.put("B4", 	12.0);
           
        values.put("C5", 	1.0);
        values.put("C#5",	2.0);
        values.put("Db5",	2.0);
        values.put("D5", 	3.0);
        values.put("D#5", 	4.0);
        values.put("Eb5", 	4.0);
        values.put("E5", 	5.0);
        values.put("F5", 	6.0);
        values.put("F#5", 	7.0);
        values.put("Gb5", 	7.0);
        values.put("G5", 	8.0);
        values.put("G#5", 	9.0);
        values.put("Ab5", 	9.0);
        values.put("A5", 	10.0);
        values.put("A#5", 	11.0);
        values.put("Bb5", 	11.0);
        values.put("B5", 	12.0);
        
        values.put("C6", 	1.0);    
        
        
        // Octave values, there are 4: 0-3
        // There is always an octave since it is assumed that either melody or harmony is present
        values.put("O1",	0.0);
        values.put("O2",	1.0);
        values.put("O3",	2.0);
        values.put("O4",	3.0);
        
        
        noteValues.put(0.0, 	"--");
        noteValues.put(1.0, 	"C3");
        noteValues.put(2.0,		"C#3");
        noteValues.put(3.0, 	"D3");
        noteValues.put(4.0, 	"D#3");
        noteValues.put(5.0, 	"E3");
        noteValues.put(6.0, 	"F3");
        noteValues.put(7.0, 	"F#3");
        noteValues.put(8.0, 	"G3");
        noteValues.put(9.0, 	"G#3");
        noteValues.put(10.0, 	"A3");
        noteValues.put(11.0, 	"A#3");
        noteValues.put(12.0, 	"B3");
        
        rhythmValues.put(1.0, 	"s");						// tiri
        rhythmValues.put(2.0,	"/0.08333333333333333");	// w^3   4/4 ti  = 3/4 tiri
        rhythmValues.put(3.0, 	"i");						// ti
        rhythmValues.put(4.0, 	"/0.16666666666666666");	// w^3 	 4/4 ta  = 3/4 ti
        rhythmValues.put(5.0, 	"i.");						// ti.
        rhythmValues.put(6.0, 	"q");						// ta
        rhythmValues.put(7.0, 	"/0.3333333333333333");		// w^3   4/4 h   = 3/4 ta
        rhythmValues.put(8.0, 	"q.");						// ta.
        rhythmValues.put(9.0, 	"h");						// ta-a
        rhythmValues.put(10.0, 	"/0.6666666666666666");		// w^3   4/4 w   = 3/4 ta-a
        rhythmValues.put(11.0, 	"h.");						// ta-a.
        rhythmValues.put(12.0, 	"w");						// tervik	
 
        octaveValues.put(0.0, 	"O1");
        octaveValues.put(1.0,	"O2");
        octaveValues.put(2.0, 	"O3");
        octaveValues.put(3.0, 	"O4");
        
        
		// Täisrütmid
		rhythmNumericValues.put("t", 1.0);  	// tiririri
		rhythmNumericValues.put("s", 2.0);  	// tiri
		rhythmNumericValues.put("i", 4.0);  	// ti
		rhythmNumericValues.put("q", 8.0);  	// ta
		rhythmNumericValues.put("h", 16.0); 	// ta-a
		rhythmNumericValues.put("w", 32.0);		// tervik
		// Poolrütmid
		rhythmNumericValues.put("s.", 3.0);  	// tiri.
		rhythmNumericValues.put("i.", 6.0);  	// ti.
		rhythmNumericValues.put("q.", 12.0); 	// ta.
		rhythmNumericValues.put("h.", 24.0); 	// ta-a.
		// Kolmandikrütmid
		rhythmNumericValues.put("/0.041666666666666664",  1.333333333333333);	// w^3 tiririri
		rhythmNumericValues.put("/0.08333333333333333",   2.666666666666667);	// w^3 tiri
		rhythmNumericValues.put("/0.16666666666666666",   5.333333333333333);  	// w^3 ti
		rhythmNumericValues.put("/0.3333333333333333",   10.66666666666667);   	// w^3 ta
		rhythmNumericValues.put("/0.6666666666666666",   21.33333333333333);  	// w^3 ta-a
		// Kolmandikkolmandik rütmid
		rhythmNumericValues.put("/0.1111111111111111",   3.55555555556);		// ta^3^3
	}
	
	
	public void transposeOctave(Melody m1, int difference){
		ArrayList<String> melodyTransposed = new ArrayList<String>();
		boolean outOfRange = false;
        for (int i = 0; i < m1.melody.size(); i++){
        	String melodyNote = m1.melody.get(i);
        	// If the current value is a pause then don't do anything
        	if(melodyNote.equals("--")){
        		melodyTransposed.add("--");
        	}
        	// If the current value is a note with a pitch, then transpose it, e.g. C4 will be C3.
        	else {
	        	int octave = Character.getNumericValue(melodyNote.charAt(melodyNote.length()-1));
	        	// 4 + (-1) = 3
	        	int newNote = octave + difference;
	        	melodyNote = melodyNote.substring(0, (melodyNote.length()-1)) + newNote;
	        	melodyTransposed.add(melodyNote);
	        	
	        	// If the newly transposed value is not in the values list, then raise a flag outOfRange.
	        	if(values.get(melodyNote) == null){
	        		outOfRange = true;
	        	}	        	
        	}
        }
        // If no flag was raised, then the original can be replaced with the transposed melody.
        if(!outOfRange){
        	m1.melody = melodyTransposed;
        }
	}
	
	public void transposeHarmony(Melody m1){
		// Harmony values are transposed to the 4th octave
		for (int i = 0; i < m1.harmony.size(); i++){
	    	m1.harmony.set(i, m1.harmony.get(i).replaceAll("\\d+.*", "4"));
		}
	}
	
	public String generateMusicString(String melody, String octave, String rhythm) {
		String musicString = "";
		
		if(melody.equals("--")){
			melody = "R"; // Jfugue musicString symbol for pause
			musicString = melody+rhythm;
		}
		else {
			melody = melody.substring(0, melody.length()-1);
			musicString = melody + parseOctaveFromOctave(octave) + rhythm;
		}
		return musicString;
	}
	
	

	
	public String parseNoteOctave(String noteString) {
		String octave = "";
		if     (noteString.endsWith("3")){ octave = "O1"; }
		else if(noteString.endsWith("4")){ octave = "O2"; }
		else if(noteString.endsWith("5")){ octave = "O3"; }
		else if(noteString.endsWith("6")){ octave = "O4"; }
		else if(noteString.equals("--")) { octave = "O1"; }
		else                             { octave = "O1"; }
		return octave;
	}
	
	public String parseOctaveFromOctave(String noteString) {
		String octave = "";
		if     (noteString.endsWith("O1")){ octave = "3"; }
		else if(noteString.endsWith("O2")){ octave = "4"; }
		else if(noteString.endsWith("O3")){ octave = "5"; }
		else if(noteString.endsWith("O4")){ octave = "6"; }
		else                              { octave = "3"; }
		return octave;
	}	

	public void mapRhythmEqualized(Melody m1) {
		for (int i = 0; i < m1.melody_RhythmEqualized.size(); i++){
			m1.melody_Mapped_RhythmEqualized.add(values.get(m1.melody_RhythmEqualized.get(i)));
			m1.melodyOctave_Mapped_RhythmEqualized.add(values.get(m1.melodyOctave_RhythmEqualized.get(i)));
			m1.melodyRhythm_Mapped_RhythmEqualized.add(values.get(m1.melodyRhythm_RhythmEqualized.get(i)));
			
			m1.harmony_Mapped_RhythmEqualized.add(values.get(m1.harmony_RhythmEqualized.get(i)));
			m1.harmonyOctave_Mapped_RhythmEqualized.add(values.get(m1.harmonyOctave_RhythmEqualized.get(i)));
			m1.harmonyRhythm_Mapped_RhythmEqualized.add(values.get(m1.harmonyRhythm_RhythmEqualized.get(i)));
		}
	}
	

	
	public void map(Melody m1) {
		// All of the melody-system notes are iterated over and assigned double values, e.g. C4 becomes 0.0.
		// Octave values are read from the note values string.
        for (int i = 0; i < m1.melody.size(); i++){
        	m1.melody_Mapped.add(values.get(m1.melody.get(i)));
        	String octaveString = parseNoteOctave(m1.melody.get(i));
        	m1.melodyOctave_Mapped.add(values.get(octaveString));
    		m1.melodyOctave.add(octaveString);        	
        }
        
        for (int i = 0; i < m1.melodyRhythm.size(); i++){
        	m1.melodyRhythm_Mapped.add(values.get(m1.melodyRhythm.get(i)));
        }
        
		for(String n : m1.melodyRhythm){
			m1.melodyRhythmNumbers.add(rhythmNumericValues.get(n));
		}
        
        for (int i = 0; i < m1.harmony.size(); i++){
        	m1.harmony_Mapped.add(values.get(m1.harmony.get(i)));
        	String octaveString = parseNoteOctave(m1.harmony.get(i));
    		m1.harmonyOctave_Mapped.add(values.get(octaveString));
    		m1.harmonyOctave.add(octaveString);        	
        }
        
        for (int i = 0; i < m1.harmonyRhythm.size(); i++){
        	m1.harmonyRhythm_Mapped.add(values.get(m1.harmonyRhythm.get(i)));
        }
        
		for(String n : m1.harmonyRhythm){
			m1.harmonyRhythmNumbers.add(rhythmNumericValues.get(n));
		}
		testMelodyMappingValidity(m1);
	}
	
	public String getKeyByValue(double value){
		for (Entry<String, Double> entry : values.entrySet()) {
	        if (entry.getValue() ==  Double.valueOf(value)) {
	        	return entry.getKey();
	        }	        	
		}
		return null;
    }
	
	public String getRhythmNameByValue(double value){
		for (Entry<String, Double> entry : rhythmNumericValues.entrySet()) {
			double diff = Math.abs(entry.getValue() - Double.valueOf(value));
	        if (diff  < 0.00000006) {
	        	return entry.getKey();
	        }	        	
		}
		return null;	
    }
	
	public double getNextSmallestRhythmByValue(double value){
		double closestSmallerValue = Double.MIN_VALUE;
		for (Entry<String, Double> entry : rhythmNumericValues.entrySet()) {
	        if (Double.valueOf(value) > entry.getValue()) {
	        	if (closestSmallerValue < entry.getValue()) {
	        		closestSmallerValue = entry.getValue();
	        	}
	        }
		}
		return closestSmallerValue;
    }
	public void testMelodyMappingValidity(Melody m){
		if(m.melody_Mapped.contains(null))			  {m.setBad(true);}
		else if(m.melodyOctave_Mapped.contains(null)) {m.setBad(true);}
		else if(m.melodyOctave.contains(null))		  {m.setBad(true);}
		else if(m.melodyRhythm_Mapped.contains(null)) {m.setBad(true);}
		else if(m.melodyRhythmNumbers.contains(null)) {m.setBad(true);}
		
		else if(m.harmony_Mapped.contains(null))	  {m.setBad(true);}
		else if(m.harmonyOctave_Mapped.contains(null)){m.setBad(true);}
		else if(m.harmonyOctave.contains(null))		  {m.setBad(true);}
		else if(m.harmonyRhythm_Mapped.contains(null)){m.setBad(true);}
		else if(m.harmonyRhythmNumbers.contains(null)){m.setBad(true);}
	}
}
