package DataReaderTranfsormerModule;
import java.util.ArrayList;


public class Melody {
	private String name;
	private boolean bad;
	
	public ArrayList<String> melody = new ArrayList<String>();
	public ArrayList<String> melodyRhythm = new ArrayList<String>();
	public ArrayList<String> harmony = new ArrayList<String>();
	public ArrayList<String> harmonyRhythm = new ArrayList<String>();
	public ArrayList<String> melodyOctave = new ArrayList<String>();
	public ArrayList<String> harmonyOctave = new ArrayList<String>();
	
	public ArrayList<Double> melody_Mapped = new ArrayList<Double>();
	public ArrayList<Double> melodyOctave_Mapped = new ArrayList<Double>();
	public ArrayList<Double> melodyRhythm_Mapped = new ArrayList<Double>();	
	public ArrayList<Double> harmony_Mapped = new ArrayList<Double>();
	public ArrayList<Double> harmonyOctave_Mapped = new ArrayList<Double>();
	public ArrayList<Double> harmonyRhythm_Mapped = new ArrayList<Double>();
	
	public ArrayList<String> melody_RhythmEqualized = new ArrayList<String>();
	public ArrayList<String> melodyOctave_RhythmEqualized = new ArrayList<String>();
	public ArrayList<String> melodyRhythm_RhythmEqualized = new ArrayList<String>();	
	public ArrayList<String> harmony_RhythmEqualized = new ArrayList<String>();
	public ArrayList<String> harmonyOctave_RhythmEqualized = new ArrayList<String>();
	public ArrayList<String> harmonyRhythm_RhythmEqualized = new ArrayList<String>();
	
	public ArrayList<Double> melody_Mapped_RhythmEqualized = new ArrayList<Double>();
	public ArrayList<Double> melodyOctave_Mapped_RhythmEqualized = new ArrayList<Double>();
	public ArrayList<Double> melodyRhythm_Mapped_RhythmEqualized = new ArrayList<Double>();	
	public ArrayList<Double> harmony_Mapped_RhythmEqualized = new ArrayList<Double>();
	public ArrayList<Double> harmonyOctave_Mapped_RhythmEqualized = new ArrayList<Double>();
	public ArrayList<Double> harmonyRhythm_Mapped_RhythmEqualized = new ArrayList<Double>();
	
	public ArrayList<Double> melodyRhythmNumbers = new ArrayList<Double>();	
	public ArrayList<Double> harmonyRhythmNumbers = new ArrayList<Double>();	
	
	public Melody(ArrayList<String> melody, ArrayList<String> melodyRhythm, ArrayList<String> harmony, ArrayList<String> harmonyRhythm) {
		this.melody = melody;
		this.melodyRhythm = melodyRhythm;
		this.harmony = harmony;
		this.harmonyRhythm = harmonyRhythm;
	}
	
	public Melody() {}	
	
	public boolean isBad() {return bad;}
	public void setBad(boolean bad) {this.bad = bad;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

}
