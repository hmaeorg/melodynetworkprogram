package examples;
import java.io.IOException;

import DataReaderTranfsormerModule.MusicDataWorker;


public class ReadAndTransformDataFromMusicXml {

	public static void main(String[] args) {
		MusicDataWorker mdw = new MusicDataWorker();
		try {
			mdw.run("learningdata/");
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
