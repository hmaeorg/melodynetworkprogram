package examples;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralRhythmPrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class FFRP_RhythmNetwork {
	public static void main(String[] args) {
		int windowSize = 17;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralRhythmPrediction();
		dataSetGenerator.setNumberOfRows(5000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralRhythmPrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.FeedForwardTYPE);
		predictionNetwork.createNetwork(new int[]{100});
		
		predictionNetwork.trainNetwork(100);

		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);
		
		predictionNetwork.saveNetwork();
		predictionNetwork.saveNetworkTraining();
	}
}
