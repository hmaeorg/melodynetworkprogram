package examples;


import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralMelodyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;

public class Recurrent_MelodyNetwork {
	public static void main(String[] args) {
		int windowSize = 1;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGenerator.setNumberOfRows(10000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
				
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.RecurrentTYPE);

		predictionNetwork.createNetwork(new int[]{80});
		
		predictionNetwork.trainNetwork(500);
	
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);
		
		predictionNetwork.saveNetwork();
		predictionNetwork.saveNetworkTraining();
	}
}