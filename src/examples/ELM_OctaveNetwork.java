package examples;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralOctavePrediction;
import NetworkTrainingModule.ElmNetworkType;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;


public class ELM_OctaveNetwork {
	public static void main(String[] args) {
		int windowSize = 17;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralOctavePrediction();
		dataSetGenerator.setNumberOfRows(1000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralOctavePrediction();
		dataSetGeneratorP.setNumberOfRows(0); //2914 on midagi viga
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);

		
		ElmNetworkType predictionNetwork =  (ElmNetworkType) NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.createNetwork(new int[]{10});
		predictionNetwork.setClassesSize(4); // Manually set the classes size
		predictionNetwork.trainNetwork(10);
		
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);

		predictionNetwork.saveNetwork();
	}
}
