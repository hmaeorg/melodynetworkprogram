package examples;
import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralMelodyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class LoadNetwork_ELM {
	public static void main(String[] args) {
		int windowSize = 17;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGenerator.setNumberOfRows(1000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.loadNetwork("elm_M_Ds3000_Hn500_Ws675_Ps1_Per0.05172413793103448_LEr1.0");

		
		// You can't use getDataArray for prediction, since it has the class row in front and
		// You have to use DataSet predictor
		//System.out.println(predictionNetwork.predict(dataSetGenerator.getDataArray()[1]));
	}
}
