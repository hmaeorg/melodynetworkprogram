package examples;


import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralOctavePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;

public class Recurrent_OctaveNetwork {
	public static void main(String[] args) {
		int windowSize = 1;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralOctavePrediction();
		dataSetGenerator.setNumberOfRows(0);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
				
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralOctavePrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.RecurrentTYPE);
		
		predictionNetwork.createNetwork(new int[]{80});
		
		predictionNetwork.trainNetwork(500);
	
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);
		
		predictionNetwork.saveNetwork();
		predictionNetwork.saveNetworkTraining();
	}
}