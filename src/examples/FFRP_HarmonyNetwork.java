package examples;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class FFRP_HarmonyNetwork {
	public static void main(String[] args) {
		int windowSize = 17;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGenerator.setNumberOfRows(5000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.FeedForwardTYPE);
		predictionNetwork.createNetwork(new int[]{100});
		
		predictionNetwork.trainNetwork(10);

		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);
		
		predictionNetwork.saveNetwork();
		predictionNetwork.saveNetworkTraining();
	}
}
