package examples;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class ELM_HarmonyNetwork {
	public static void main(String[] args) {
		int windowSize = 17;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGenerator.setNumberOfRows(1000);
		dataSetGenerator.setPurifyDataSet(true);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.createNetwork(new int[]{500});
		
		predictionNetwork.trainNetwork(100);

		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);

		predictionNetwork.saveNetwork();
	}
}
