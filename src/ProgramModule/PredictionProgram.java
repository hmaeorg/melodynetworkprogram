package ProgramModule;

import org.apache.commons.lang3.ArrayUtils;
import org.encog.ml.data.MLDataSet;
import org.jfugue.pattern.Pattern;
import org.jfugue.player.Player;

public abstract class PredictionProgram {
	String melodyMusicString;
	String harmonyMusicString;
	
	MLDataSet inputData;
	
	public void loadInputData(MLDataSet input){
		inputData = input;
	}
	
	public void predictNextN(int n){
		for (int i = 0; i < n; i++){
			predictNext();
		}
	}
	
	public abstract void predictNext();
	
	public void play(){
		System.out.println(melodyMusicString);
		System.out.println(harmonyMusicString);
	    Pattern p1 = new Pattern("V0 I[Piano] "+melodyMusicString);
	    Pattern p2 = new Pattern("V1 I[Piano] "+harmonyMusicString);
	    Player player = new Player();
	    player.play(p1, p2);
	}
	
	public double[] generateInputBlock(double[] predictorBlock){
		double[] inputBlock 	= inputData.get(inputData.size()-1).getInput().getData(); //Gets last row in DataSet
		double[] newInputBlock 	= ArrayUtils.addAll(inputBlock,predictorBlock);
		return newInputBlock;		
	}
}
