package ProgramModule;

import DataReaderTranfsormerModule.MusicDataMapOneOctave;


import NetworkTrainingModule.PredictionNetwork;

import java.util.Arrays;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;
import org.encog.mathutil.Equilateral;
import org.encog.ml.data.basic.BasicMLData;

import util.Util;

public class MelodyPredictionProgram extends PredictionProgram {
	PredictionNetwork melodyNetwork;
	PredictionNetwork octaveNetwork;
	PredictionNetwork rhythmNetwork;
	
	public void loadMelodyNetwork(PredictionNetwork network){
		melodyNetwork = network;
	}
	
	public void loadOctaveNetwork(PredictionNetwork network){
		octaveNetwork = network;
	}
	
	public void loadRhythmNetwork(PredictionNetwork network){
		rhythmNetwork = network;
	}

	public void predictNext(){
		Scanner user_input = new Scanner(System.in);
		String harmonyString = user_input.next();
		//user_input.close();
		
		Equilateral harmonyEquilateral = new Equilateral(13,0,1);
		Equilateral melodyEquilateral  = new Equilateral(13,0,1);
		Equilateral octaveEquilateral  = new Equilateral(4,0,1);
		Equilateral rhythmEquilateral  = new Equilateral(13,0,1);
		
		
		MusicDataMapOneOctave mdMap = new MusicDataMapOneOctave();
		int harmonyNoteDecoded = mdMap.values.get(harmonyString).intValue();

		double[] harmony 	= harmonyEquilateral.encode(harmonyNoteDecoded);
		double[] melody 	= Util.normalizeN(predictMelody(harmony), 13);
		double[] octave 	= Util.normalizeN(predictOctave(ArrayUtils.addAll(melody,harmony)), 4);
		double[] rhythm 	= Util.normalizeN(predictRhythm(ArrayUtils.addAll(ArrayUtils.addAll(melody,harmony),octave)), 13);
		
		int melodyNoteDecoded 	= melodyEquilateral.decode(melody);
		int octaveDecoded 		= octaveEquilateral.decode(octave);
		int rhythmDecoded 		= rhythmEquilateral.decode(rhythm);		
		
		String melodyNoteString = mdMap.noteValues.get((double)melodyNoteDecoded);
		String octaveString 	= mdMap.octaveValues.get((double)octaveDecoded);
		String rhythmString 	= mdMap.rhythmValues.get((double)rhythmDecoded);
		
		System.out.println("melodyNoteDecoded " + melodyNoteDecoded);
		System.out.println("melodyNoteString " 	+melodyNoteString);
		System.out.println("octaveString " 		+octaveString);
		System.out.println("rhythmString " 		+rhythmString);
		
		System.out.println(mdMap.generateMusicString(melodyNoteString, octaveString, rhythmString));
		System.out.println(mdMap.generateMusicString(melodyNoteString, octaveString, rhythmString).replaceAll("\\d+.*", "3"));
		
		melodyMusicString = melodyMusicString + " " +mdMap.generateMusicString(melodyNoteString, octaveString, rhythmString);
		harmonyMusicString = harmonyMusicString + " " +harmonyString.replaceAll("\\d+.*", "3")+rhythmString;
		
		//create next dataset input
		double[] dataBeginning 	= Arrays.copyOfRange(inputData.get(inputData.size()-1).getInputArray(), 39, inputData.get(0).getInputArray().length);
		double[] dataEnding 	= ArrayUtils.addAll(ArrayUtils.addAll(ArrayUtils.addAll(melody,harmony),octave),rhythm);
		inputData.add(new BasicMLData(ArrayUtils.addAll(dataBeginning,dataEnding)));
	}
	
	public double predictMelody(double[] predictorBlock){
		double[] newInputBlock 	= generateInputBlock(predictorBlock);
		return melodyNetwork.predict(newInputBlock);
	}
	
	public double predictOctave(double[] predictorBlock){
		double[] newInputBlock 	= generateInputBlock(predictorBlock);
		return octaveNetwork.predict(newInputBlock);
	}
	
	public double predictRhythm(double[] predictorBlock){
		double[] newInputBlock 	= generateInputBlock(predictorBlock);
		return rhythmNetwork.predict(newInputBlock);
	}
}


