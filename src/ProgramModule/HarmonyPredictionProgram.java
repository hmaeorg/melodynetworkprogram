package ProgramModule;

import DataReaderTranfsormerModule.MusicDataMapOneOctave;



import NetworkTrainingModule.PredictionNetwork;


import java.util.Arrays;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;
import org.encog.mathutil.Equilateral;
import org.encog.ml.data.basic.BasicMLData;

import util.Util;

public class HarmonyPredictionProgram extends PredictionProgram {
	PredictionNetwork harmonyNetwork;
	
	public void loadHarmonyNetwork(PredictionNetwork network){
		harmonyNetwork = network;
	}

	public void predictNext(){
		Scanner user_input = new Scanner(System.in);
		String melodyOctaveRhythmString = user_input.next();

		//user_input.close();
		String melodyNoteString = "";
		String octaveString = "";
		String rhythmString = "";
		if(melodyOctaveRhythmString.length()==4){
			melodyNoteString = melodyOctaveRhythmString.substring(0, 3);
			octaveString = "O"+ melodyOctaveRhythmString.substring(2, 3);
			rhythmString = melodyOctaveRhythmString.substring(3, 4);
		}
		else if(melodyOctaveRhythmString.length()==3){
			melodyNoteString = melodyOctaveRhythmString.substring(0, 2);
			octaveString = "O"+ melodyOctaveRhythmString.substring(1, 2);
			rhythmString = melodyOctaveRhythmString.substring(2, 3);
		}
		else {}
		
		melodyNoteString = melodyNoteString.toUpperCase();
		octaveString = octaveString.toUpperCase();
		rhythmString = rhythmString.toLowerCase();

		Equilateral melodyEquilateral  = new Equilateral(13,0,1);
		Equilateral octaveEquilateral  = new Equilateral(4,0,1);
		Equilateral rhythmEquilateral  = new Equilateral(13,0,1);
		
		MusicDataMapOneOctave mdMap = new MusicDataMapOneOctave();
		int melodyNoteDecoded 	= mdMap.values.get(melodyNoteString).intValue();
		int octavDecoded 		= mdMap.values.get(octaveString).intValue();
		int rhythmDecoded 		= mdMap.values.get(rhythmString).intValue();

		double[] melody 	= melodyEquilateral.encode(melodyNoteDecoded);
		double[] octave 	= octaveEquilateral.encode(octavDecoded);
		double[] rhythm 	= rhythmEquilateral.encode(rhythmDecoded);
		double[] harmony    = Util.normalizeN(predictHarmony(ArrayUtils.addAll(ArrayUtils.addAll(melody,octave),rhythm)),13);

		int harmonyNoteDecoded 	= melodyEquilateral.decode(harmony);
		String harmonyNoteString = mdMap.noteValues.get((double)harmonyNoteDecoded);

		
		System.out.println("harmonyNoteString " + harmonyNoteString);

		
		System.out.println(mdMap.generateMusicString(melodyNoteString, octaveString, rhythmString));
		System.out.println(mdMap.generateMusicString(harmonyNoteString, octaveString, rhythmString).replaceAll("\\d+.*", "3"));
		
		melodyMusicString = melodyMusicString + " " +mdMap.generateMusicString(melodyNoteString, octaveString, rhythmString);
		harmonyMusicString = harmonyMusicString + " " +harmonyNoteString.replaceAll("\\d+.*", "3")+rhythmString;
		
		//create next dataset input
		double[] dataBeginning 	= Arrays.copyOfRange(inputData.get(inputData.size()-1).getInputArray(), 39, inputData.get(0).getInputArray().length);
		double[] dataEnding 	= ArrayUtils.addAll(ArrayUtils.addAll(ArrayUtils.addAll(melody,harmony),octave),rhythm);
		inputData.add(new BasicMLData(ArrayUtils.addAll(dataBeginning,dataEnding)));
	}
	

	
	public double predictHarmony(double[] predictorBlock){
		double[] newInputBlock 	= generateInputBlock(predictorBlock);
		return harmonyNetwork.predict(newInputBlock);
	}
}


