package util;

import java.util.ArrayList;
import java.util.List;

import org.encog.mathutil.Equilateral;

public class Util {
	public static double[] convertDoubles(List<Double> doubles){
	    double[] ret = new double[doubles.size()];
	    for(int i = 0; i < ret.length; i++) {
	    	if(doubles.get(i)==null){
	    		doubles.set(i, 999.0);
	    	}
	    	ret[i] = doubles.get(i).doubleValue();
	    }
	    return ret;
	}
	
	
	public static void addAllElementsFromArrayToArraylist(List<Double> doubleList, double[] doubleArr){
	    for (int i=0; i<doubleArr.length; i++){
	    	doubleList.add(doubleArr[i]);
	    }
	}
	
	public static double[] normalizeN(double number, int space){
		Equilateral eq = new Equilateral(space,0,1);
		double[] d = eq.encode((int)number);
		return d;
	}
	
	public static ArrayList<ArrayList<double[]>> toEquilateral(List<String[]> list, int space){
		ArrayList<ArrayList<double[]>> allRows = new ArrayList<ArrayList<double[]>>();
		for(String[] row : list){
			ArrayList<double[]> rowEquilateral = new ArrayList<double[]>();
			for(int i = 0; i < row.length; i++){
				rowEquilateral.add(Util.normalizeN(Double.parseDouble(row[i]), space));
			}
			allRows.add(rowEquilateral);
		}
		return allRows;
	}	
	
	public static double convertEquilateralToNormal(double[] result, int space){
		Equilateral noteE = new Equilateral(space, 0, 1);
		return noteE.decode(result);
	}
}
