package util;

public class Paths {
	private static String dataDir = "data/";
	private static String csvDir = "data/csv/";
	private static String learningdataDir = "data/csv/learningdata";
	private static String testingdataDir = "data/csv/testingdata";
	private static String predictfromdataDir = "data/csv/predictfromdata";
	private static String musciXMLDir = "data/musicXml/";
	private static String networksDir = "data/networks/";
	private static String modelsDir = "data/networks/models/";
	private static String savesDir = "data/networks/saves/";
	private static String learningDataDir = "data/csv/learningdata/";
	private static String testingDataDir = "data/csv/testingdata/";
	private static String predictFromDataDir = "data/csv/predictfromdata/";
	
	public static String getDataDir() {
		return dataDir;
	}
	public static void setDataDir(String dataDir) {
		Paths.dataDir = dataDir;
	}
	public static String getCsvDir() {
		return csvDir;
	}
	public static void setCsvDir(String csvDir) {
		Paths.csvDir = csvDir;
	}
	public static String getMusciXMLDir() {
		return musciXMLDir;
	}
	public static void setMusciXMLDir(String musciXMLDir) {
		Paths.musciXMLDir = musciXMLDir;
	}
	public static String getNetworksDir() {
		return networksDir;
	}
	public static void setNetworksDir(String networksDir) {
		Paths.networksDir = networksDir;
	}
	public static String getModelsDir() {
		return modelsDir;
	}
	public static void setModelsDir(String modelsDir) {
		Paths.modelsDir = modelsDir;
	}
	public static String getSavesDir() {
		return savesDir;
	}
	public static void setSavesDir(String savesDir) {
		Paths.savesDir = savesDir;
	}
	public static String getLearningDataDir() {
		return learningDataDir;
	}
	public static void setLearningDataDir(String learningDataDir) {
		Paths.learningDataDir = learningDataDir;
	}
	public static String getTestingDataDir() {
		return testingDataDir;
	}
	public static void setTestingDataDir(String testingDataDir) {
		Paths.testingDataDir = testingDataDir;
	}
	public static String getPredictFromDataDir() {
		return predictFromDataDir;
	}
	public static void setPredictFromDataDir(String predictFromDataDir) {
		Paths.predictFromDataDir = predictFromDataDir;
	}
	public static String getLearningdataDir() {
		return learningdataDir;
	}
	public static void setLearningdataDir(String learningdataDir) {
		Paths.learningdataDir = learningdataDir;
	}
	public static String getTestingdataDir() {
		return testingdataDir;
	}
	public static void setTestingdataDir(String testingdataDir) {
		Paths.testingdataDir = testingdataDir;
	}
	public static String getPredictfromdataDir() {
		return predictfromdataDir;
	}
	public static void setPredictfromdataDir(String predictfromdataDir) {
		Paths.predictfromdataDir = predictfromdataDir;
	}
}
