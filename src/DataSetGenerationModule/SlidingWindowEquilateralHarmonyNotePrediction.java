package DataSetGenerationModule;

import java.util.ArrayList;
import util.Util;


public class SlidingWindowEquilateralHarmonyNotePrediction extends DataSetGenerator {	
	public void fillInputBlock(ArrayList<Double> window, int melodyIndex, int position){
		Util.addAllElementsFromArrayToArraylist(window, melody_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
		Util.addAllElementsFromArrayToArraylist(window, harmony_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
		Util.addAllElementsFromArrayToArraylist(window, melodyOctave_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
		Util.addAllElementsFromArrayToArraylist(window, melodyRhythm_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
	}
	
	public void fillPredictorBlock(ArrayList<Double> predictor, int melodyIndex, int position){
		Util.addAllElementsFromArrayToArraylist(predictor, melody_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
		Util.addAllElementsFromArrayToArraylist(predictor, melodyOctave_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
		Util.addAllElementsFromArrayToArraylist(predictor, melodyRhythm_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
	}
	
	public void fillPredictionBlock(ArrayList<Double> prediction, int melodyIndex, int position){
		Util.addAllElementsFromArrayToArraylist(prediction, harmony_Mapped_RhythmEqualized_Equilateral.get(melodyIndex).get(position));
	}
}