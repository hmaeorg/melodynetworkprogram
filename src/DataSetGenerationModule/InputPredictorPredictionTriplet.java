package DataSetGenerationModule;

import java.util.ArrayList;

public class InputPredictorPredictionTriplet {
	private ArrayList<Double> window = new ArrayList<Double>();
	private ArrayList<Double> predictor = new ArrayList<Double>();
	private ArrayList<Double> prediction = new ArrayList<Double>();
	
	public ArrayList<Double> getWindow() {return window;}
	public ArrayList<Double> getPredictor() {return predictor;}
	public ArrayList<Double> getPrediction() {return prediction;}
	
	public InputPredictorPredictionTriplet(ArrayList<Double> windowA, ArrayList<Double> predictorA, ArrayList<Double> predictionA) {
		window = windowA;
		predictor = predictorA;
		prediction = predictionA;
	}
}
