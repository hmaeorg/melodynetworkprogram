package DataSetGenerationModule;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;








import org.apache.commons.lang3.ArrayUtils;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;

import util.Paths;
import util.Util;

import com.opencsv.CSVReader;


public abstract class DataSetGenerator {
	protected int windowSize;
	protected int predictionSize;
	
	private MLDataSet learningData;
	private MLDataSet predictionData;
	
	private int numberOfRowsToRead;
	private boolean purifyDataSet;
	
	private List<String[]> melody_Mapped_RhythmEqualized;
	private List<String[]> melodyOctave_Mapped_RhythmEqualized;
	private List<String[]> melodyRhythm_Mapped_RhythmEqualized;
	
	private List<String[]> harmony_Mapped_RhythmEqualized;
	private List<String[]> harmonyOctave_Mapped_RhythmEqualized;
	private List<String[]> harmonyRhythm_Mapped_RhythmEqualized;
	
	protected ArrayList<ArrayList<double[]>> melody_Mapped_RhythmEqualized_Equilateral;
	protected ArrayList<ArrayList<double[]>> melodyOctave_Mapped_RhythmEqualized_Equilateral;
	protected ArrayList<ArrayList<double[]>> melodyRhythm_Mapped_RhythmEqualized_Equilateral;
	
	protected ArrayList<ArrayList<double[]>> harmony_Mapped_RhythmEqualized_Equilateral;
	protected ArrayList<ArrayList<double[]>> harmonyOctave_Mapped_RhythmEqualized_Equilateral;
	protected ArrayList<ArrayList<double[]>> harmonyRhythm_Mapped_RhythmEqualized_Equilateral;
	
	private ArrayList<InputPredictorPredictionTriplet> inputPredictorPredictionTriplets = new ArrayList<InputPredictorPredictionTriplet>();	
	
	public void readData(String path){
	    try {
			CSVReader melody_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"melody_Mapped_RhythmEqualized.csv"));
			melody_Mapped_RhythmEqualized = melody_Mapped_RhythmEqualizedReader.readAll();			
			
			CSVReader melodyOctave_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"melodyOctave_Mapped_RhythmEqualized.csv"));
			melodyOctave_Mapped_RhythmEqualized = melodyOctave_Mapped_RhythmEqualizedReader.readAll();
			
			CSVReader melodyRhythm_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"melodyRhythm_Mapped_RhythmEqualized.csv"));
			melodyRhythm_Mapped_RhythmEqualized = melodyRhythm_Mapped_RhythmEqualizedReader.readAll();			
			
			CSVReader harmony_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"harmony_Mapped_RhythmEqualized.csv"));
			harmony_Mapped_RhythmEqualized = harmony_Mapped_RhythmEqualizedReader.readAll();
			
			CSVReader harmonyOctave_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"harmonyOctave_Mapped_RhythmEqualized.csv"));
			harmonyOctave_Mapped_RhythmEqualized = harmonyOctave_Mapped_RhythmEqualizedReader.readAll();
			
			CSVReader harmonyRhythm_Mapped_RhythmEqualizedReader = new CSVReader(new FileReader(Paths.getCsvDir()+path+"harmonyRhythm_Mapped_RhythmEqualized.csv"));
			harmonyRhythm_Mapped_RhythmEqualized = harmonyRhythm_Mapped_RhythmEqualizedReader.readAll();	
		
			melody_Mapped_RhythmEqualizedReader.close();
			melodyOctave_Mapped_RhythmEqualizedReader.close();
			melodyRhythm_Mapped_RhythmEqualizedReader.close();
			
			harmony_Mapped_RhythmEqualizedReader.close();
			harmonyOctave_Mapped_RhythmEqualizedReader.close();
			harmonyRhythm_Mapped_RhythmEqualizedReader.close();
		} catch (IOException e) {}
	}

	public void convertDataToEquilateral(){				
		melody_Mapped_RhythmEqualized_Equilateral =  		Util.toEquilateral(melody_Mapped_RhythmEqualized, 13);
		melodyOctave_Mapped_RhythmEqualized_Equilateral = 	Util.toEquilateral(melodyOctave_Mapped_RhythmEqualized, 4);
		melodyRhythm_Mapped_RhythmEqualized_Equilateral = 	Util.toEquilateral(melodyRhythm_Mapped_RhythmEqualized, 13);
		
		harmony_Mapped_RhythmEqualized_Equilateral = 		Util.toEquilateral(harmony_Mapped_RhythmEqualized, 13);
		harmonyOctave_Mapped_RhythmEqualized_Equilateral = 	Util.toEquilateral(harmonyOctave_Mapped_RhythmEqualized, 4);
		harmonyRhythm_Mapped_RhythmEqualized_Equilateral = 	Util.toEquilateral(harmonyRhythm_Mapped_RhythmEqualized, 13);	
	}
	
	public InputPredictorPredictionTriplet generateInputPredictorPredictionRow(int windowSize, int predictionSize, int shift, int melodyIndex){
		ArrayList<Double> window = new ArrayList<Double>();
		ArrayList<Double> predictor = new ArrayList<Double>();
		ArrayList<Double> prediction = new ArrayList<Double>();

		boolean badData = false;
		int end = windowSize + predictionSize + shift +1;
		for(int j = shift; j < end; j++){
			try{ // Try to shift the window forward
				if(j > windowSize+shift){ // block to predict
					fillPredictionBlock(prediction, melodyIndex, j);
				}
				else if(j == windowSize+shift){ // Predictor block
					fillPredictorBlock(predictor, melodyIndex, j);
				}
				else { // Input block
					fillInputBlock(window, melodyIndex, j);
				}
			}
			catch(Exception e){ // If the window cannot be shifted
				badData = true;
			}
		}	
		if(!badData){
			return new InputPredictorPredictionTriplet(window, predictor, prediction);
		}
		return null;
	}
	
	public void generateDataSet(String path, int inputWindowSize, int predictionWindowSize){
		this.windowSize = inputWindowSize;
		this.predictionSize = predictionWindowSize;
		
		readData(path);
		convertDataToEquilateral();
		prepeareDataForSpecificWindowSize(inputWindowSize, predictionWindowSize);
		
		if(isPurifyDataSet()){
			removeDuplicateRowsFromDataSet();	
		}
		System.out.println("Block sizes: " +inputPredictorPredictionTriplets.get(0).getWindow().size() + " " + inputPredictorPredictionTriplets.get(0).getPredictor().size() + " " + inputPredictorPredictionTriplets.get(0).getPrediction().size());
		
		double[][] allWindowsArray = new double[inputPredictorPredictionTriplets.size()][inputPredictorPredictionTriplets.get(0).getWindow().size()+inputPredictorPredictionTriplets.get(0).getPredictor().size()];
		double[][] allPredictionsArray = new double[inputPredictorPredictionTriplets.size()][inputPredictorPredictionTriplets.get(0).getPrediction().size()];
		for(int i = 0; i < inputPredictorPredictionTriplets.size(); i++){
			allWindowsArray[i] = ArrayUtils.addAll(Util.convertDoubles(inputPredictorPredictionTriplets.get(i).getWindow()),Util.convertDoubles(inputPredictorPredictionTriplets.get(i).getPredictor()));
			allPredictionsArray[i] = Util.convertDoubles(inputPredictorPredictionTriplets.get(i).getPrediction());
		}		
		learningData = new BasicMLDataSet(allWindowsArray, allPredictionsArray);
		System.out.println("Dataset size: " + learningData.size());
	}
	
	
	public void generatePredictionSet(String path, int inputWindowSize, int predictionWindowSize){
		this.windowSize = inputWindowSize;
		this.predictionSize = predictionWindowSize;
		readData(path);
		convertDataToEquilateral();
		prepeareDataForSpecificWindowSize(inputWindowSize, predictionWindowSize);
		
		double[][] allWindowsArray = new double[inputPredictorPredictionTriplets.size()][inputPredictorPredictionTriplets.get(0).getWindow().size()];
		
		for(int i = 0; i < inputPredictorPredictionTriplets.size(); i++){
			allWindowsArray[i] = Util.convertDoubles(inputPredictorPredictionTriplets.get(i).getWindow());
		}
		predictionData = new BasicMLDataSet(allWindowsArray, allWindowsArray);
	}
	
	
	public void prepeareDataForSpecificWindowSize(int windowSize, int predictionSize){
		int rowCounter =0;
		
		for(int i = 0; i < melody_Mapped_RhythmEqualized_Equilateral.size(); i++){
			boolean canShift = true;
			boolean limitNotReached = true;
			int shift = 1;
			
			while(canShift && limitNotReached){
				if(rowCounter==numberOfRowsToRead && numberOfRowsToRead !=0){
					break;
				}
				InputPredictorPredictionTriplet row = generateInputPredictorPredictionRow(windowSize, predictionSize, shift, i);
				if(!(row == null)){
					inputPredictorPredictionTriplets.add(row);
				}
				else {
					break;
				}
				shift++;
				rowCounter++;
			}
		}
	}
	
	public void removeDuplicateRowsFromDataSet(){
		Set<ArrayList<Double>> purifiedInputSet = new LinkedHashSet<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> badInput = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> badPrediction = new ArrayList<ArrayList<Double>>();
		ArrayList<InputPredictorPredictionTriplet> inputPredictionPairsPure = new ArrayList<InputPredictorPredictionTriplet>();
		boolean isNotIn = true;
		
		for(int i = 0; i < inputPredictorPredictionTriplets.size(); i++){
			ArrayList<Double> inpPredic = new ArrayList<Double>();
			inpPredic.addAll(inputPredictorPredictionTriplets.get(i).getWindow());
			inpPredic.addAll(inputPredictorPredictionTriplets.get(i).getPredictor());
			
			isNotIn = purifiedInputSet.add(inpPredic);
					
			if(isNotIn){
				InputPredictorPredictionTriplet pair = new InputPredictorPredictionTriplet(inputPredictorPredictionTriplets.get(i).getWindow(), inputPredictorPredictionTriplets.get(i).getPredictor(), inputPredictorPredictionTriplets.get(i).getPrediction());
				inputPredictionPairsPure.add(pair);
			}
			else {
				badInput.add(inpPredic);
				badPrediction.add(inputPredictorPredictionTriplets.get(i).getPrediction());
			}
		}
		System.out.println("purified: " + inputPredictionPairsPure.size() + "		" + "previous: " + inputPredictorPredictionTriplets.size());
		inputPredictorPredictionTriplets = inputPredictionPairsPure;
	}
	
	public double[][] getTrainingDataForElm(){
		int rows = learningData.size();
		int cols = learningData.getInputSize()+learningData.getIdealSize();
		
		double[][] allData = new double[rows][cols];
		for(int i = 0; i < rows; i++){
			double [] predic = new double[1];			
			predic[0] = Util.convertEquilateralToNormal(learningData.get(i).getIdealArray(), learningData.get(i).getIdealArray().length+1); // 1-column class for classification 0 - n.
			allData[i] = ArrayUtils.addAll(predic, learningData.get(i).getInputArray());
		}	
		return allData;
	}
	
	public int getWindowSize() {return windowSize;}
	public void setWindowSize(int windowSize) {this.windowSize = windowSize;}
	public int getPredictionSize() {return predictionSize;}
	public void setPredictionSize(int predictionSize) {this.predictionSize = predictionSize;}
	public int getNumberOfRows() { return numberOfRowsToRead;}
	public void setNumberOfRows(int numberOfRows) { this.numberOfRowsToRead = numberOfRows;}
	public MLDataSet getDataSet() {return learningData;}
	public MLDataSet getPredictionDataSet() {return predictionData;}
	public boolean isPurifyDataSet() {return purifyDataSet;}
	public void setPurifyDataSet(boolean purifyDataSet) {this.purifyDataSet = purifyDataSet;}
	
	public abstract void fillInputBlock(ArrayList<Double> window, int melodyIndex, int position);
	public abstract void fillPredictorBlock(ArrayList<Double> predictor, int melodyIndex, int position);
	public abstract void fillPredictionBlock(ArrayList<Double> prediction, int melodyIndex, int position);
}
