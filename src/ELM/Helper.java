package ELM;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.opencsv.CSVReader;

import no.uib.cipr.matrix.DenseMatrix;

public class Helper {
	public static void printDenseMatrix(DenseMatrix input) {
		for(int i = 0; i < input.numRows(); i++){
			for(int j = 0; j < input.numColumns(); j++){
				System.out.print(input.get(i, j) + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
		System.out.println();
	}
	
	public static void writeNetworkSettingsToFile(int NumberofInputNeurons,
												  int NumberofHiddenNeurons,
												  int NumberofOutputNeurons, 
												  String func, 
												  int elmType,
												  String name,
												  String path) throws IOException {
		File outputFile = new File(path+name+".csv");
		
		FileWriter fw = new FileWriter(outputFile);
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write(NumberofInputNeurons+ "," +NumberofHiddenNeurons+ "," +NumberofOutputNeurons+ "," +func+ "," +elmType);
		bw.newLine();	
		
		bw.close();
		fw.close();
	}
	

	
	public static void writeDenseMatrixToFile(DenseMatrix input, String name, String path) throws IOException {
		File outputFile = new File(path+name+".csv");
		
		FileWriter fw = new FileWriter(outputFile);
		BufferedWriter bw = new BufferedWriter(fw);

		String[] array = toStringArray(denseMatrixToArray(input));
		
		for(int i = 0; i < array.length; i++){
			bw.write(array[i]);
			bw.newLine();	
		}
		bw.close();
		fw.close();
	}
	
	public static String[] loadNetworkSettings(String filename) throws IOException{
		CSVReader csvreader = new CSVReader(new FileReader(filename));
		List<String[]> input = csvreader.readAll();
		String[] settings = new String[input.get(0).length];
		System.out.println(Arrays.toString(input.get(0)));
		settings[0] = input.get(0)[0];
		settings[1] = input.get(0)[1];
		settings[2] = input.get(0)[2];
		settings[3] = input.get(0)[3];
		settings[4] = input.get(0)[4];
		return settings;
	}
	
	public static double[][] denseMatrixToArray(DenseMatrix input) {
		double[][] array = new double[input.numRows()][input.numColumns()];
		for(int i = 0; i < input.numRows(); i++){
			for(int j = 0; j < input.numColumns(); j++){
				array[i][j] = input.get(i, j);
			}
		}
		return array;
	}
	
	public static String[] toStringArray(double[][] input) {
		String[] array = new String[input.length];

		for(int i = 0; i < input.length; i++){
			String a = "";
			for(int j = 0; j < input[0].length; j++){
				a = a + String.valueOf(input[i][j]) + ",";
			}
			array[i] = a.toString().substring(0, a.length() - 1); //eemaldame viimase koma
		}
		return array;
	}
	
	public static double[][] toDoubleArray(List<String[]> input) {
		double[][] array = new double[input.size()][input.get(0).length];

		for(int i = 0; i < input.size(); i++){
			for(int j = 0; j < input.get(0).length; j++){
				array[i][j] =Double.parseDouble(input.get(i)[j]);

			}
		}
		return array;
	}
	
	public static DenseMatrix loadDenseMatrix(String filename) throws IOException{
		CSVReader csvreader = new CSVReader(new FileReader(filename));
		List<String[]> input = csvreader.readAll();		
		

		double[][] inputArray = toDoubleArray(input);
		DenseMatrix matrix = new DenseMatrix(inputArray);
		
		return matrix;
	}
	
	public static int[] generateLabel(int size) {	
		int[] array = new int[size];
		for(int i = 0; i < size; i++){
			array[i] = i;
		}
		return array;
	}
}
