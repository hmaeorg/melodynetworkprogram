package main;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class ELM_HarmonyNetwork {
	public static void main(String[] args) {
		int windowSize = Integer.parseInt(args[0]);
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGenerator.setNumberOfRows(0);
		dataSetGenerator.setPurifyDataSet(true);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGenerator.setNumberOfRows(0);
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.createNetwork(new int[]{Integer.parseInt(args[1])});
		
		predictionNetwork.trainNetwork(100);
		System.out.println("Training time: " + predictionNetwork.getTrainingTime());
		
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);

		predictionNetwork.saveNetwork();
	}
}
