package main;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralOctavePrediction;
import DataSetGenerationModule.SlidingWindowEquilateralRhythmPrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class ELM_OctaveNetwork {
	public static void main(String[] args) {
		int windowSize = Integer.parseInt(args[0]);
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralOctavePrediction();
		dataSetGenerator.setNumberOfRows(0);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralOctavePrediction();
		dataSetGeneratorP.setNumberOfRows(0); //2914 on midagi viga, kuna elm v�rgus on hetkel kirjas nii, et otsitakse ise klasside arv, siis kui andmetes pole nt klass nr 3, siis testides on siis on viga.
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);

		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.createNetwork(new int[]{Integer.parseInt(args[1])});
		
		predictionNetwork.trainNetwork(1);
		
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);

		predictionNetwork.saveNetwork();
	}
}
