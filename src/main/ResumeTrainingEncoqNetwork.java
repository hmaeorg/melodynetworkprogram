package main;


import java.util.concurrent.TimeUnit;

import org.encog.util.Stopwatch;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralMelodyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;

public class ResumeTrainingEncoqNetwork {
	public static void main(String[] args) {
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGenerator.generateDataSet("learningdata/", 17, 1);

		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.RecurrentTYPE);
		predictionNetwork.loadNetwork ("MPN_I100_Ds4939_Hn100_Ws17_Ps1_Per0.159476_Er0.021022.eg");
		predictionNetwork.loadTraining("MPN_I100_Ds4939_Hn100_Ws17_Ps1_Per0.159476_Er0.021022.ser");
		
		Stopwatch timer = new Stopwatch();
		timer.start();
		predictionNetwork.resumeTrainNetwork(10);
		timer.stop();
		System.out.println("Elapsed time: " + TimeUnit.MILLISECONDS.toSeconds(timer.getElapsedMilliseconds())+" seconds");
	}
}