package main;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralMelodyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;


public class ELM_MelodyNetwork {
	public static void main(String[] args) {
		int windowSize = Integer.parseInt(args[0]);
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGenerator.setNumberOfRows(0);
		dataSetGenerator.setPurifyDataSet(true);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
		
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralMelodyNotePrediction();
		dataSetGeneratorP.setNumberOfRows(0);
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.ElmTYPE);
		predictionNetwork.createNetwork(new int[]{Integer.parseInt(args[1])});
		
		predictionNetwork.trainNetwork(100);
		
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);

		predictionNetwork.saveNetwork();
	}
}
