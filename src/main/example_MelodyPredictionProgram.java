package main;



import org.encog.ml.data.MLDataSet;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.PredictionNetwork;
import NetworkTrainingModule.ElmTypeNetworks.Melody_ELM;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Octave_FF_RP;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Rhythm_FF_RP;
import ProgramModule.MelodyPredictionProgram;

public class example_MelodyPredictionProgram {
	public static void main(String[] args) {
//		MusicDataWorker mdw = new MusicDataWorker();
//		try {
//			mdw.run("predictfromdataCSV/");
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}

		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		System.out.println("JOUOUO");
		dataSetGenerator.generatePredictionSet("predictfromdata/", 17, 1);
		MLDataSet dataset = dataSetGenerator.getPredictionDataSet();
		
		MelodyPredictionProgram predictor = new MelodyPredictionProgram();
		
		// Load melody note prediction network
		PredictionNetwork mNet = new Melody_ELM(dataSetGenerator);
		mNet.loadNetwork("elm_M_Ds3000_Hn500_Ws675_Ps1_Per0.05747126436781609_LEr1.0");
		
		// Load melody octave prediction network
		PredictionNetwork oNet = new Octave_FF_RP(dataSetGenerator);
		oNet.loadNetwork("ffrp_O_I100_Ds174_Hn100_Ws17_Ps3_Per0.6609195402298851_Ler1.2171421389769942E-4.eg");
		
		// Load rhythm prediction network
		PredictionNetwork rNet = new Rhythm_FF_RP(dataSetGenerator);
		rNet.loadNetwork("ffrp_R_I100_Ds30_Hn100_Ws17_Ps12_Per0.21264367816091953_Ler0.0021935760305753305.eg");
		
		// Set networks
		predictor.loadMelodyNetwork(mNet);
		predictor.loadOctaveNetwork(oNet);
		predictor.loadRhythmNetwork(rNet);
		predictor.loadInputData(dataset);
		
		// Predict
		predictor.predictNextN(10);
		predictor.play();
	}
}