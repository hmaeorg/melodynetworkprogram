package main;


import java.io.IOException;

import org.encog.ml.data.MLDataSet;

import DataReaderTranfsormerModule.MusicDataWorker;
import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.PredictionNetwork;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Harmony_FF_RP;
import ProgramModule.HarmonyPredictionProgram;

public class example_HarmonyPredictionProgram {
	public static void main(String[] args) {
		MusicDataWorker mdw = new MusicDataWorker();
		try {
			mdw.run("predictfromdata/");
		} catch (IOException e) {

			e.printStackTrace();
		}

		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		System.out.println("JOUOUO");
		dataSetGenerator.generateDataSet("predictfromdata/", 17, 1);
		MLDataSet dataset = dataSetGenerator.getPredictionDataSet();
		
		HarmonyPredictionProgram predictor = new HarmonyPredictionProgram();
		
		// Load melody note prediction network
		PredictionNetwork hNet = new Harmony_FF_RP(dataSetGenerator);
		hNet.loadNetwork("ffrp_H_I100_Ds30_Hn100_Ws17_Ps1_Per0.3_Ler0.01821079013299253.eg");
		
		predictor.loadHarmonyNetwork(hNet);
		predictor.loadInputData(dataset);
		
		predictor.predictNextN(10);
		predictor.play();
	}
}