package main;


import java.util.concurrent.TimeUnit;

import org.encog.util.Stopwatch;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import NetworkTrainingModule.NetworkFactory;
import NetworkTrainingModule.NetworkTypes;
import NetworkTrainingModule.PredictionNetwork;

public class Recurrent_HarmonyNetwork {
	public static void main(String[] args) {
		int windowSize = 1;
		int predictionSize = 1;
		
		DataSetGenerator dataSetGenerator = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGenerator.setNumberOfRows(10000);
		dataSetGenerator.generateDataSet("learningdata/", windowSize, predictionSize);
				
		DataSetGenerator dataSetGeneratorP = new SlidingWindowEquilateralHarmonyNotePrediction();
		dataSetGeneratorP.generateDataSet("testingdata/", windowSize, predictionSize);
		
		PredictionNetwork predictionNetwork = NetworkFactory.getNetwork(dataSetGenerator, NetworkTypes.RecurrentTYPE);
		
		predictionNetwork.createNetwork(new int[]{80});
		
		predictionNetwork.trainNetwork(500);
		
		predictionNetwork.evaluateNetwork(dataSetGenerator);
		predictionNetwork.evaluateNetwork(dataSetGeneratorP);
		
		predictionNetwork.saveNetwork();
		predictionNetwork.saveNetworkTraining();
	}
}