package NetworkTrainingModule;

import DataSetGenerationModule.DataSetGenerator;
import DataSetGenerationModule.SlidingWindowEquilateralHarmonyNotePrediction;
import DataSetGenerationModule.SlidingWindowEquilateralMelodyNotePrediction;
import DataSetGenerationModule.SlidingWindowEquilateralOctavePrediction;
import DataSetGenerationModule.SlidingWindowEquilateralRhythmPrediction;
import NetworkTrainingModule.ElmTypeNetworks.Harmony_ELM;
import NetworkTrainingModule.ElmTypeNetworks.Melody_ELM;
import NetworkTrainingModule.ElmTypeNetworks.Octave_ELM;
import NetworkTrainingModule.ElmTypeNetworks.Rhythm_ELM;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Harmony_FF_RP;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Melody_FF_RP;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Octave_FF_RP;
import NetworkTrainingModule.encoqTypeNetworks.FeedForward.Rhythm_FF_RP;
import NetworkTrainingModule.encoqTypeNetworks.Recurrent.Harmony_Elman;
import NetworkTrainingModule.encoqTypeNetworks.Recurrent.Melody_Elman;
import NetworkTrainingModule.encoqTypeNetworks.Recurrent.Octave_Elman;
import NetworkTrainingModule.encoqTypeNetworks.Recurrent.Rhythm_Elman;

public class NetworkFactory {
	public static PredictionNetwork getNetwork(DataSetGenerator a, NetworkTypes type){
		PredictionNetwork network = null;
		if(type == NetworkTypes.FeedForwardTYPE){
			if(a instanceof SlidingWindowEquilateralHarmonyNotePrediction){
				System.out.println("FeedForwardTYPE: SlidingWindowEquilateralHarmonyNotePrediction");
				network = new Harmony_FF_RP(a);
			}
			else if(a instanceof SlidingWindowEquilateralMelodyNotePrediction){
				System.out.println("FeedForwardTYPE: SlidingWindowEquilateralMelodyNotePrediction");
				network = new Melody_FF_RP(a);
			}
			else if(a instanceof SlidingWindowEquilateralOctavePrediction){
				System.out.println("FeedForwardTYPE: SlidingWindowEquilateralOctavePrediction");
				network = new Octave_FF_RP(a);
			}
			else if(a instanceof SlidingWindowEquilateralRhythmPrediction){
				System.out.println("FeedForwardTYPE: SlidingWindowEquilateralRhythmPrediction");
				network = new Rhythm_FF_RP(a);
			}			
		}
		else if(type == NetworkTypes.RecurrentTYPE){
			if(a instanceof SlidingWindowEquilateralHarmonyNotePrediction){
				System.out.println("RecurrentTYPE: SlidingWindowEquilateralHarmonyNotePrediction");
				network = new Harmony_Elman(a);
			}
			else if(a instanceof SlidingWindowEquilateralMelodyNotePrediction){
				System.out.println("RecurrentTYPE: SlidingWindowEquilateralMelodyNotePrediction");
				network = new Melody_Elman(a);
			}
			else if(a instanceof SlidingWindowEquilateralOctavePrediction){
				System.out.println("RecurrentTYPE: SlidingWindowEquilateralOctavePrediction");
				network = new Octave_Elman(a);
			}
			else if(a instanceof SlidingWindowEquilateralRhythmPrediction){
				System.out.println("RecurrentTYPE: SlidingWindowEquilateralRhythmPrediction");
				network = new Rhythm_Elman(a);
			}
		}
		else if(type == NetworkTypes.ElmTYPE){
			if(a instanceof SlidingWindowEquilateralHarmonyNotePrediction){
				System.out.println("ElmTYPE: SlidingWindowEquilateralHarmonyNotePrediction");
				network = new Harmony_ELM(a);
			}
			else if(a instanceof SlidingWindowEquilateralMelodyNotePrediction){
				System.out.println("ElmTYPE: SlidingWindowEquilateralMelodyNotePrediction");
				network = new Melody_ELM(a);
			}
			else if(a instanceof SlidingWindowEquilateralOctavePrediction){
				System.out.println("ElmTYPE: SlidingWindowEquilateralOctavePrediction");
				network = new Octave_ELM(a);
			}
			else if(a instanceof SlidingWindowEquilateralRhythmPrediction){
				System.out.println("ElmTYPE: SlidingWindowEquilateralRhythmPrediction");
				network = new Rhythm_ELM(a);
			}
		}
		return network;
	}
}