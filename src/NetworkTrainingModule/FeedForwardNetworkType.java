package NetworkTrainingModule;



import java.util.concurrent.TimeUnit;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.util.Stopwatch;

import DataSetGenerationModule.DataSetGenerator;

public abstract class FeedForwardNetworkType extends EncoqNetwork {

	public FeedForwardNetworkType(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}

	@Override
	public void createNetwork(int[] layers){
		hiddenLayerSize = layers[0];

	    network = new BasicNetwork();
	    network.addLayer(new BasicLayer(null, true, dataSet.getInputSize()));
	    network.addLayer(new BasicLayer(new ActivationSigmoid(), true, layers[0]));
	    if(layers.length > 1){network.addLayer(new BasicLayer(new ActivationSigmoid(), true, layers[1]));}	  
	    network.addLayer(new BasicLayer(new ActivationSigmoid(), false, dataSet.getIdealSize()));
	    network.getStructure().finalizeStructure();
	    network.reset();
	}
	
	@Override
	public void trainNetwork(int toIteration){
		this.toIteration = toIteration;

		final ResilientPropagation train = new ResilientPropagation(network, dataSet);
		train.setThreadCount(0);
		
		double smallestError = 100;
		double currentError = 100;
		int epoch = 1;
		int epochCounter = 0;
		Stopwatch timer = new Stopwatch();
		timer.start();
		do {
			train.iteration();
			currentError = train.getError();
			if(currentError < smallestError){
				smallestError = currentError;
			}
			if(epochCounter==5){
				System.out.println("Epoch #" + epoch + " 	Error:" + currentError + "	Smallest error: "+ smallestError);
				epochCounter=0;
			}
			epoch++;
			epochCounter++;
		} while(epoch != toIteration);
		networkError = currentError;
		cont = train.pause();
		train.finishTraining();
		Encog.getInstance().shutdown();
		timer.stop();
		trainingTime = TimeUnit.MILLISECONDS.toSeconds(timer.getElapsedMilliseconds())+" seconds";
	}
}
