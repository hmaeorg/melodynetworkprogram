package NetworkTrainingModule;

import java.util.concurrent.TimeUnit;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.CalculateScore;
import org.encog.ml.train.MLTrain;
import org.encog.ml.train.strategy.HybridStrategy;
import org.encog.ml.train.strategy.StopTrainingStrategy;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.TrainingSetScore;
import org.encog.neural.networks.training.anneal.NeuralSimulatedAnnealing;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.pattern.ElmanPattern;


import org.encog.util.Stopwatch;

import DataSetGenerationModule.DataSetGenerator;

public abstract class ElmanNetworkType extends EncoqNetwork {

	public ElmanNetworkType(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}
	
	@Override
	public void createNetwork(int[] layers){
		hiddenLayerSize = layers[0];

		ElmanPattern pattern = new ElmanPattern();
		pattern.setActivationFunction(new ActivationTANH());
		pattern.setInputNeurons(dataSet.getInputSize());
		pattern.addHiddenLayer(layers[0]);  
		pattern.setOutputNeurons(dataSet.getIdealSize());
		network = (BasicNetwork) pattern.generate();
	}
	
	@Override
	public void trainNetwork(int toIteration){
		this.toIteration = toIteration;
		
		CalculateScore score=new TrainingSetScore(dataSet);
		final MLTrain trainAlt=new NeuralSimulatedAnnealing(network,score,10,2,10);
		final ResilientPropagation train = new ResilientPropagation(network, dataSet);
		train.setThreadCount(0);
		final StopTrainingStrategy stop = new StopTrainingStrategy();
		final MLTrain trainMain=train;
		
		trainMain.addStrategy(new HybridStrategy(trainAlt));
		trainMain.addStrategy(stop);

		double smallestError = 100;
		double currentError = 100;
		int epoch = 1;
		int epochCounter = 0;
		
		Stopwatch timer = new Stopwatch();
		timer.start();
		while(epoch != toIteration) {
			trainMain.iteration();
			currentError = trainMain.getError();
			if(currentError < smallestError){
				smallestError = currentError;
			}
			if(epochCounter==5){
				System.out.println("Epoch #" + epoch + " 	Error:" + currentError + "	Smallest error: "+ smallestError);
				epochCounter=0;
			}
			epoch++;
			epochCounter++;
		}
		networkError = currentError;
		cont = trainMain.pause();
		trainMain.finishTraining();
		Encog.getInstance().shutdown();
		timer.stop();
		trainingTime = TimeUnit.MILLISECONDS.toSeconds(timer.getElapsedMilliseconds())+" seconds";
	}
}
