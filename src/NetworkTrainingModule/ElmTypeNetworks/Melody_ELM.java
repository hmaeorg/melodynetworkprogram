package NetworkTrainingModule.ElmTypeNetworks;



import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmNetworkType;

public class Melody_ELM extends ElmNetworkType {
	public Melody_ELM(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}

	

	public String getName() {
		return "elm_M";
	}
}
