package NetworkTrainingModule.ElmTypeNetworks;

import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmNetworkType;

public class Rhythm_ELM extends ElmNetworkType {
	public Rhythm_ELM(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}

	public String getName() {
		return "elm_R";
	}
}
