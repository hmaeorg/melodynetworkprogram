package NetworkTrainingModule.ElmTypeNetworks;

import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmNetworkType;

public class Octave_ELM extends ElmNetworkType {
	public Octave_ELM(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}

	public String getName() {
		return "elm_O";
	}
}
