package NetworkTrainingModule.ElmTypeNetworks;

import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmNetworkType;

public class Harmony_ELM extends ElmNetworkType {
	public Harmony_ELM(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}

	public String getName() {
		return "elm_H";
	}
}
