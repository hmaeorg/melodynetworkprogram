package NetworkTrainingModule;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.NotConvergedException;

import org.apache.commons.lang3.ArrayUtils;

import util.Paths;

import com.opencsv.CSVReader;

import DataSetGenerationModule.DataSetGenerator;
import ELM.Helper;
import ELM.elm;

public abstract class ElmNetworkType extends PredictionNetwork {
	protected elm elmNetwork;
	
	public ElmNetworkType(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}
	
	@Override
	public String generateSettingsString() {
		return  "_Ds"  + dataSetGenerator.getNumberOfRows()+ 
				"_Hn" + elmNetwork.getNumberofHiddenNeurons()+ 
				"_Ws" + elmNetwork.getNumberofInputNeurons()+ 
				"_Ps" + dataSetGenerator.getPredictionSize()+ 
				"_Per"+ String.valueOf(getTestingSuccessRate())+ 
				"_LEr"+ String.valueOf(getNetworkError());
	}


	@Override
	public void saveNetwork() {
		this.networkName = getName()+generateSettingsString();
		String path = Paths.getModelsDir()+networkName+"/";
		File dir = new File(Paths.getModelsDir()+networkName);
		dir.mkdir();
		try {
			Helper.writeDenseMatrixToFile(elmNetwork.getInputWeight(), "InputWeight", path);
			Helper.writeDenseMatrixToFile(elmNetwork.getBiasofHiddenNeurons(), "BiasofHiddenNeurons", path);
			Helper.writeDenseMatrixToFile(elmNetwork.getOutputWeight(), "OutputWeight", path);
			Helper.writeNetworkSettingsToFile(elmNetwork.getNumberofInputNeurons(), 
											  elmNetwork.getNumberofHiddenNeurons(), 
											  elmNetwork.getNumberofOutputNeurons(), 
											  elmNetwork.getFunc(), 
											  elmNetwork.getElm_Type(),
											  "Settings",
											  path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveNetworkTraining() {
		System.out.println("Can't save training with Elm");
		
	}

	@Override
	public void loadTraining(String name) {
		System.out.println("Can't load training with Elm");
	}
	
	@Override
	public void loadNetwork(String name) {
		String path = Paths.getModelsDir()+name+"/";
		try {
			DenseMatrix inputWeight = 			Helper.loadDenseMatrix(path+"InputWeight.csv");
			DenseMatrix biasOfHiddenNeurons = 	Helper.loadDenseMatrix(path+"BiasofHiddenNeurons.csv");
			DenseMatrix outputWeight = 			Helper.loadDenseMatrix(path+"OutputWeight.csv");
			String[] settings = 				Helper.loadNetworkSettings(path+"Settings.csv");
			int numberOfInputNeurons = 	Integer.parseInt(settings[0]);
			int numberOfHiddenNeurons = Integer.parseInt(settings[1]);
			int numberOfOutputNeurons = Integer.parseInt(settings[2]);
			String func = settings[3];
			int elmType = Integer.parseInt(settings[4]); 
			
			elmNetwork = new elm();
			elmNetwork.setInputWeight(inputWeight);
			elmNetwork.setBiasofHiddenNeurons(biasOfHiddenNeurons);
			elmNetwork.setOutputWeight(outputWeight);
			elmNetwork.setNumberofInputNeurons(numberOfInputNeurons);
			elmNetwork.setNumberofHiddenNeurons(numberOfHiddenNeurons);
			elmNetwork.setNumberofOutputNeurons(numberOfOutputNeurons);
			elmNetwork.setFunc(func);
			elmNetwork.setElm_Type(elmType);
			if(elmType==1){
				int[]label = Helper.generateLabel(Integer.parseInt(settings[2]));
				elmNetwork.setLabel(label);	
			}
		} catch (IOException e1) {}
	}
	
	public static String[] loadNetworkSettings(String filename) throws IOException{
		CSVReader csvreader = new CSVReader(new FileReader(filename));
		List<String[]> input = csvreader.readAll();
		csvreader.close();
		String[] settings = new String[input.get(0).length];
		System.out.println(Arrays.toString(input.get(0)));
		settings[0] = input.get(0)[0];
		settings[1] = input.get(0)[1];
		settings[2] = input.get(0)[2];
		settings[3] = input.get(0)[3];
		settings[4] = input.get(0)[4];
		
		return settings;
	}

	@Override
	public void evaluateNetwork(DataSetGenerator evalDataSetGenerator) {
		System.out.println();
		System.out.println();
		System.out.println(evalDataSetGenerator.getTrainingDataForElm()[0].length);
		elmNetwork.test(evalDataSetGenerator.getTrainingDataForElm());		

		double all = (double)evalDataSetGenerator.getTrainingDataForElm().length;
		double correct = (double)evalDataSetGenerator.getTrainingDataForElm().length-elmNetwork.getMissClassificationRate_Testing();
		double successRate = correct / all ;
		
		testingSetPredictionSuccessRate = successRate;
		System.out.println("Data Rows: " + all);
		System.out.println("Success rate: " + successRate);
	}

	@Override
	public void createNetwork(int[] layers) {
		elmNetwork = new elm(1, layers[0], "sin");
	}

	@Override
	public void trainNetwork(int toIteration) {
		System.out.println("Elm network doesn't do iterative training");
		try {
			elmNetwork.train(dataSetGenerator.getTrainingDataForElm());
		} catch (NotConvergedException e) {
			e.printStackTrace();
		}		
		networkError = elmNetwork.getTrainingAccuracy();		
		trainingTime = elmNetwork.getTrainingTime()+" seconds";
	}

	public void setClassesSize(int numberofOutputNeurons) {
		elmNetwork.setNumberofOutputNeurons(numberofOutputNeurons);
	}

	@Override
	public void resumeTrainNetwork(int toIteration) {
		System.out.println("Can't resume training with Elm");
	}	
	
	@Override
	public double predict(double[] row) {
		// The classification column is set to zero, it doesn't change anything but the elm source code needs the input vector to be of certain size.
		double[][] a = {ArrayUtils.addAll(new double[]{0.0},row)};
		System.out.println(elmNetwork.testOut(a)[0]);
		return elmNetwork.testOut(a)[0];
	}
}
