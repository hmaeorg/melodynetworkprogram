package NetworkTrainingModule.encoqTypeNetworks.Recurrent;


import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmanNetworkType;

public class Rhythm_Elman extends ElmanNetworkType {
	public Rhythm_Elman(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
		predictionSize = 12;
	}

	@Override
	public String getName() {
		return "rec_M";
	}
}
