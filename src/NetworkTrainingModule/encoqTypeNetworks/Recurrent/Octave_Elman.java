package NetworkTrainingModule.encoqTypeNetworks.Recurrent;



import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.ElmanNetworkType;

public class Octave_Elman extends ElmanNetworkType {
	public Octave_Elman(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
		predictionSize = 3;
	}

	@Override
	public String getName() {
		return "rec_M";
	}
}
