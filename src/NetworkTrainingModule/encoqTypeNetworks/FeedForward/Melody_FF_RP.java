package NetworkTrainingModule.encoqTypeNetworks.FeedForward;



import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.FeedForwardNetworkType;

public class Melody_FF_RP extends FeedForwardNetworkType {
	public Melody_FF_RP(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
		predictionSize = 12;
	}
	
	@Override
	public String getName() {
		return "ffrp_M";
	}
}
