package NetworkTrainingModule.encoqTypeNetworks.FeedForward;



import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.FeedForwardNetworkType;


public class Octave_FF_RP extends FeedForwardNetworkType {

	public Octave_FF_RP(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
		predictionSize = 3;
	}
	
	@Override
	public String getName() {
		return "ffrp_O";
	}
}
