package NetworkTrainingModule.encoqTypeNetworks.FeedForward;



import DataSetGenerationModule.DataSetGenerator;
import NetworkTrainingModule.FeedForwardNetworkType;

public class Rhythm_FF_RP extends FeedForwardNetworkType {

	public Rhythm_FF_RP(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
		predictionSize = 12;
	}

	@Override
	public String getName() {
		return "ffrp_R";
	}
}
