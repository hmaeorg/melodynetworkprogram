package NetworkTrainingModule;

import java.io.File;
import java.io.IOException;

import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.propagation.TrainingContinuation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.obj.SerializeObject;

import util.Paths;
import util.Util;
import DataSetGenerationModule.DataSetGenerator;

public abstract class EncoqNetwork extends PredictionNetwork {
	protected BasicNetwork network;
	protected MLDataSet dataSet = dataSetGenerator.getDataSet();;

	
	protected TrainingContinuation cont;	
	protected int hiddenLayerSize;
	protected int toIteration;
	protected int predictionSize;
	
	public EncoqNetwork(DataSetGenerator dataSetGenerator) {
		super(dataSetGenerator);
	}
	
	public int getWindowSize() {return windowSize;}
	public void setWindowSize(int windowSize) {this.windowSize = windowSize;}
	public int getPredictionSize() {return predictionSize;}
	public void setPredictionSize(int predictionSize) {this.predictionSize = predictionSize;}
	public int getHiddenLayerSize() {return hiddenLayerSize;}
	public int getToIteration() {return toIteration;}
	
	
	@Override
	public String generateSettingsString() {
		return  "_I" + getToIteration() + 
				"_Ds" + dataSet.size()+ 
				"_Hn" + getHiddenLayerSize()+ 
				"_Ws" + getWindowSize()+ 
				"_Ps" + getPredictionSize()+ 
				"_Per"+ String.valueOf(getTestingSuccessRate())+ 
				"_Ler" + String.valueOf(getNetworkError());
	}

	@Override
	public void loadNetwork(String name){
		network = (BasicNetwork)EncogDirectoryPersistence.loadObject(new File(Paths.getModelsDir()+name));
	}
	@Override
	public void loadTraining(String name){
		try {
			cont = (TrainingContinuation)SerializeObject.load(new File(Paths.getSavesDir()+name));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveNetwork() {
		this.networkName = getName()+generateSettingsString();
		EncogDirectoryPersistence.saveObject(new File(Paths.getModelsDir()+ this.networkName+".eg"),network);
		System.out.println(this.networkName+".eg");
	}
	
	@Override
	public void saveNetworkTraining() {
		this.networkName = getName()+generateSettingsString();
		try {
			System.out.println(networkName+".ser");
			SerializeObject.save(new File(Paths.getSavesDir()+networkName+".ser"), cont);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	@Override
	public void evaluateNetwork(DataSetGenerator evalDataSetGenerator){
		MLDataSet testSet = evalDataSetGenerator.getDataSet();
		int correctPredictions = 0;
		for(MLDataPair pair: testSet ) {
			final MLData output = network.compute(pair.getInput());
			double expectedvalue = Util.convertEquilateralToNormal(pair.getIdealArray(), predictionSize+1);
			double realvalue = Util.convertEquilateralToNormal(output.getData(), predictionSize+1);
			
			if(expectedvalue==realvalue){
				correctPredictions++;
			}
		}
		double all = (double)testSet.size();
		double correct = (double)correctPredictions;
		double successRate = correct / all ;
		testingSetPredictionSuccessRate = successRate;
		System.out.println("Data Rows: " + all);
		System.out.println("Success rate: " + successRate);
	}
	
	@Override
	public double predict(double[] row) {
		return (Double) Util.convertEquilateralToNormal(network.compute(new BasicMLData(row)).getData(), predictionSize+1);
	}
	
	@Override
	public void resumeTrainNetwork(int toIteration){
		this.toIteration = toIteration;
		final ResilientPropagation train = new ResilientPropagation(network, dataSet);
		train.setThreadCount(0);
		train.resume(cont);
		double smallestError = 100;
		double currentError = 100;
		int epoch = 1;
		int counter = 0;
		do {
			train.iteration();
			currentError = train.getError();
			if(currentError < smallestError){
				smallestError = currentError;
			}
			if(counter ==5){
				System.out.println("Epoch #" + epoch + " 	Error:" + currentError + "	Smallest error: "+ smallestError);
				counter=0;
			}
			epoch++;
			counter++;
		} while(epoch != toIteration);
		cont = train.pause();
		train.finishTraining();
		Encog.getInstance().shutdown();
	}
}
