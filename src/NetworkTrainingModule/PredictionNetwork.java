package NetworkTrainingModule;


import DataSetGenerationModule.DataSetGenerator;




public abstract class PredictionNetwork {
	protected DataSetGenerator dataSetGenerator;
	protected String networkName;
	protected double networkError;
	protected double testingSetPredictionSuccessRate;
	protected int windowSize;
	protected int predictionSize;
	protected String trainingTime;

	public PredictionNetwork(DataSetGenerator dataSetGenerator) {
		this.dataSetGenerator = dataSetGenerator;
		this.windowSize = dataSetGenerator.getWindowSize();
		this.predictionSize = dataSetGenerator.getPredictionSize();
	}
		
	public double getTestingSuccessRate() {return testingSetPredictionSuccessRate;}
	public double getNetworkError() {return networkError;}
	public String getTrainingTime() {return trainingTime;}
	
	public abstract String getName();
	public abstract void evaluateNetwork(DataSetGenerator evalDataSetGenerator);
	public abstract void createNetwork(int[] layers);
	public abstract void trainNetwork(int toIteration);
	public abstract void saveNetwork();
	public abstract void saveNetworkTraining();
	public abstract void resumeTrainNetwork(int toIteration);
	public abstract void loadNetwork(String name);
	public abstract void loadTraining(String name);
	public abstract double predict(double[] row);
	public abstract String generateSettingsString();
}